import 'dart:async';

import 'package:analyzer/dart/element/element.dart';
import 'package:build/build.dart';
import 'package:source_gen/source_gen.dart';
import 'package:spot_db_library/spot_db_library.dart';

class SpotDbLibraryReporterGenerator extends GeneratorForAnnotation<SpotDb> {
  @override
  FutureOr<String> generateForAnnotatedElement(Element element, ConstantReader annotation, BuildStep buildStep) {
    return "// Hey! Annotation found!";
  }


}
