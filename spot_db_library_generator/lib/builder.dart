import 'package:build/build.dart';
import 'package:source_gen/source_gen.dart';
import 'src/spot_db_library_generator.dart';

Builder spotDbReporter(BuilderOptions options) =>
    SharedPartBuilder([SpotDbLibraryReporterGenerator()], 'spot_db_library');