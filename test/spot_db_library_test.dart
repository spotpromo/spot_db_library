import 'package:spot_db_library/spot_db_library.dart';
import 'package:test/test.dart';

void main() {
  group('DB annotation', () {
    test('must have a non-null name', () {
      expect(() => SpotDb(), throwsA(TypeMatcher<AssertionError>()));
    });

    test('does not need to have a todoUrl', () {
      final database = SpotDb(table: 'teste');
      expect(database.table, null);
    });

    test('if it is a given a todoUrl, it will be part of the model', () {
      final givenTable = 'teste';
      final todo = SpotDb(table: 'teste', field: 'teste', type: 'INTEGER');
      expect(todo.field, givenTable);
    });
  });
}
