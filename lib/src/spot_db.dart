class SpotDb {
  final String? table;
  final String? field;
  final String? type;

  const SpotDb({this.table, this.field, this.type}) :assert(table != null);
}